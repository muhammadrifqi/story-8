$(document).ready(function() {

    $.ajax({
        method: 'GET',
        url : 'https://www.googleapis.com/books/v1/volumes?q=Pemrograman Web',
        success: function(response) {
            $('tbody').empty();
            for(let i=0; i < response.items.length; i++) {
                var row = document.createElement('tr');
                $(row).append("<td class='title text-center'><a class='fontblack' target='_blank' href='"+ response.items[i].volumeInfo.infoLink +"'>" + response.items[i].volumeInfo.title + "</a></td>");
                try {
                    $(row).append('<td class="desc">' + response.items[i].volumeInfo.description + '</td>');
                }
                catch {
                    $(row).append('<td class="desc">Tidak ada deskripsi</td>');
                }
                $(row).append('<td class="link text-center">' + response.items[i].volumeInfo.authors + '</td>')
                try {
                    $(row).append('<td class="img text-center"><img src="' + response.items[i].volumeInfo.imageLinks.thumbnail + '"></td>');
                }
                catch(e) {
                    $(row).append('<td class="img text-center">Tidak ada foto</td>')
                }
                $('tbody').append(row);
            }
        }
    })

     $('#button-addon1').click(function() {
        let key  = $('#boxsearch').val();
        $.ajax({
            method: 'GET',
            url : 'https://www.googleapis.com/books/v1/volumes?q=' + key,
            success: function(response) {
                $('tbody').empty();
                for(let i=0; i < response.items.length; i++) {
                    var row = document.createElement('tr');
                    $(row).append("<td class='title text-center'><a class='fontblack' target='_blank' href='"+ response.items[i].volumeInfo.infoLink +"'>" + response.items[i].volumeInfo.title + "</a></td>");

                    try {
                        $(row).append('<td class="desc">' + response.items[i].volumeInfo.description + '</td>');
                    }
                    catch {
                        $(row).append('<td class="desc">Tidak ada deskripsi</td>');
                    }
                    $(row).append('<td class="link text-center">' + response.items[i].volumeInfo.authors + '</td>')
                    try {
                        $(row).append('<td class="img text-center"><img src="' + response.items[i].volumeInfo.imageLinks.thumbnail + '"></td>');
                    }
                    catch(e) {
                        $(row).append('<td class="img text-center">Tidak ada foto</td>')
                    }
                    $('tbody').append(row);
                }
            }
        })
    })
})