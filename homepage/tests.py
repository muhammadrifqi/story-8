from django.test import TestCase, Client
from django.urls import resolve
from homepage.views import homepage
from django.http import HttpRequest
# Create your tests here.


class UnitTestStory8(TestCase):
	def test_landingpage_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'homepage.html')

	def test_notexist_url_is_notexist(self):
		response = Client().get('/gaada/')
		self.assertEqual(response.status_code, 404)

	def test_homepage_using_homepage_function(self):
		response = resolve('/')
		self.assertEqual(response.func, homepage)

	def test_landing_page_title_is_right(self):
		request = HttpRequest()
		response = homepage(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<title>Story-8 Rifqi</title>', html_response)
